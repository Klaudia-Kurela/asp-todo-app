﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace todo_domain_entities
{
    public class TodoList
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public IQueryable ShowAllLists()
        {
            using (var db = new ToDoContext())
            {
                var lists = db.ToDoLists;
                return lists;
            }
        }

        public void AddList(string name)
        {
            using (var db = new ToDoContext())
            {
                var taskList = new Lists { Name = name };
                db.ToDoLists.Add(taskList);
                db.SaveChanges();
            }
        }

        public void AddTask(string listName, string taskName, string? taskDescription = null, DateTime? dueDate = null, Status taskStatus = Status.NotStarted)
        {
            using (var db = new ToDoContext())
            {
                var taskList = db.ToDoLists.Where(x => x.Name.Equals(listName)).Select(x => x).FirstOrDefault();
                var newTask = new Entries
                {
                    TaskStatus = taskStatus,
                    TaskTitle = taskName,
                    TaskDescription = taskDescription,
                    TaskCreationDate = DateTime.Now,
                    TaskDueDate = (DateTime)dueDate,
                    ListName = taskList,
                    ListId = taskList.ListId
                };

                db.ToDoTasks.Add(newTask);
                db.SaveChanges();
            }
        }

        public void ModifyEntry(string listName, string taskName, string? newTitle = null, string? newDescription = null, DateTime? newDueDate = null, Status newTaskStatus = Status.NotStarted, Lists newList = null)
        {
            if (newTitle == string.Empty)
            {
                throw new ArgumentNullException(nameof(newTitle));
            }

            using (var db = new ToDoContext())
            {
                var editetTask = db.ToDoTasks.Where(x => x.ListName.Name.Equals(listName)).Where(y => y.TaskTitle.Equals(taskName)).FirstOrDefault();
                if (editetTask != null)
                {
                    if (newTitle != null)
                    {
                        editetTask.TaskTitle = newTitle;
                    }

                    if (newDescription != null && newDescription != string.Empty)
                    {
                        editetTask.TaskDescription = newDescription;
                    }

                    if (newDueDate != null)
                    {
                        editetTask.TaskDueDate = (DateTime)newDueDate;
                    }

                    if (newTaskStatus != null) 
                    {
                        editetTask.TaskStatus = newTaskStatus;
                    }

                    if (newList != null)
                    {
                        editetTask.ListName = newList;
                    }

                    db.ToDoTasks.Update(editetTask);
                    db.SaveChanges();
                }
            }
        }

        public void ModifyList(string listName, string? newName = null)
        {
            if (newName == string.Empty)
            {
                throw new ArgumentNullException(nameof(newName));
            }

            using (var db = new ToDoContext())
            {
                var firstList = db.ToDoLists.Where(x => x.Name.Equals(listName)).FirstOrDefault();
                List<Entries> tasks = null;
                if (firstList != null)
                {
                    if (newName != null)
                    {
                        tasks = ExistTasks(firstList);
                        firstList.Name = newName;
                    }

                    if (tasks != null)
                    {
                        foreach (var t in tasks)
                        {
                            ModifyEntry(listName, t.TaskTitle, newList: firstList);
                        }
                    }

                    db.ToDoLists.Update(firstList);
                    db.SaveChanges();
                }
            }
        }

        public void UpdateStatus(string listName, string taskName, Status newStatus)
        {
            //Use enum!
            if (newStatus == Status.Completed)
            {
                ModifyEntry(listName, taskName, newTaskStatus: Status.Completed);
            }
            
            if (newStatus == Status.InProgress)
            {
                ModifyEntry(listName, taskName, newTaskStatus: Status.InProgress);
            }
            
            if (newStatus == Status.NotStarted)
            {
                ModifyEntry(listName, taskName, newTaskStatus: Status.NotStarted);
            }

            //Invalid argument exception
            throw new ArgumentException("Uknown status");
        }

        public List<Entries> ExistTasks(Lists taskList)
        {
            using (var db = new ToDoContext())
            {
                return db.ToDoTasks.Where(x => x.ListName.Equals(taskList)).ToList();
            }
        }
    }
}
