﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace todo_domain_entities
{
    public class Entries
    {
        [Key]
        public int TaskId { get; set; }

        public Status TaskStatus { get; set; }

        public string TaskTitle { get; set; }

        public string TaskDescription { get; set; }

        public DateTime TaskCreationDate { get; set; }

        public DateTime TaskDueDate { get; set; }


        public int ListId { get; set; }

        public virtual Lists ListName { get; set; }

    }
}
