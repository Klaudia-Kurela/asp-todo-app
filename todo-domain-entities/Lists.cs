﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace todo_domain_entities
{
    public class Lists
    {
        [Key]
        public int ListId { get; set; }

        public string Name { get; set; }

        public virtual List<Entries> ToDoList { get; set; }
    }
}
