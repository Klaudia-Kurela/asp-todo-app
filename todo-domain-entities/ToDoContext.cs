﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace todo_domain_entities
{
    public class ToDoContext : DbContext
    {
        private DbContextOptionsBuilder opBuilter;
        public DbSet<Lists> ToDoLists { get; set; }

        public DbSet<Entries> ToDoTasks { get; set; }

        public ToDoContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database= tododatabase;Trusted_Connection=True;");
        }
    }
}
